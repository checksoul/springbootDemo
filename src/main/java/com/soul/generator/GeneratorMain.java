package com.soul.generator;

import org.mybatis.generator.ant.GeneratorAntTask;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.exception.InvalidConfigurationException;
import org.mybatis.generator.exception.XMLParserException;
import org.mybatis.generator.internal.DefaultShellCallback;
import org.mybatis.generator.internal.NullProgressCallback;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 1.使用main方法方式生成mybatis的文件，需要添加依赖：
 * --------------
 * <dependency>
 *     <groupId>org.mybatis.generator</groupId>
 *     <artifactId>mybatis-generator-core</artifactId>
 *     <version>1.3.6</version>
 * </dependency>
 * --------------
 * 2.使用命令行的方式：java -jar mybatis-generator-core-xxx.jar -configfile generator.xml -overwrite
 *      使用命令行的方式需要将配置文件中的targetProject路径改成绝对路径
 * 3.使用maven插件的方式运行 mybatis-generator:generate，需要添加插件：
 * --------------
 * <plugin>
 *     <groupId>org.mybatis.generator</groupId>
 *     <artifactId>mybatis-generator-maven-plugin</artifactId>
 *     <version>1.3.5</version>
 *     <configuration>
 *         <configurationFile>${basedir}/src/main/resources//mybatis-generator/generatorConfig.xml</configurationFile>
 *         <overwrite>true</overwrite>
 *         <verbose>true</verbose>
 *     </configuration>
 * </plugin>
 * --------------
 */
public class GeneratorMain {
    /**
     * 自动生成代码主类
     * @param args
     */
    public static void main(String[] args){
        List<String> warnings = new ArrayList<>();
        boolean overwrite = true;
        //获取配置文件
        String genCfg = "/mybatis-generator/generatorConfig.xml";
        File configFile = new File(GeneratorMain.class.getResource(genCfg).getFile());
        //加载配置文件
        ConfigurationParser cp = new ConfigurationParser(warnings);
        Configuration config = null;
        try {
            config = cp.parseConfiguration(configFile);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        } catch (XMLParserException e) {
            e.printStackTrace();
            return;
        }

        DefaultShellCallback callback = new DefaultShellCallback(overwrite);
        //创建生成类
        MyBatisGenerator myBatisGenerator = null;
        try {
            myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
            return;
        }

        try {
            //开始生成文件
            myBatisGenerator.generate(null);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
