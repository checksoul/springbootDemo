package com.soul.springboot.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * spring boot 的主类
 */
@SpringBootApplication
public class SoulMain {

    public static void main(String[] args){
        SpringApplication.run(SoulMain.class,args);
    }
}
