package com.soul.blog.service.impl;

import com.soul.blog.dao.TestEntityMapper;
import com.soul.blog.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TestServiceImpl implements TestService {

    @Autowired
    private TestEntityMapper testEntityMapper;

    @Override
    public String select() {
        return testEntityMapper.selectByPrimaryKey(1).toString();
    }
}
