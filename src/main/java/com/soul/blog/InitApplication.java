package com.soul.blog;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(value = "com.soul.blog.dao")
public class InitApplication {
    public static void main(String[] args){
        SpringApplication.run(InitApplication.class,args);
    }
}
