DROP TABLE IF EXISTS test;
CREATE TABLE test (
`id`  int NOT NULL AUTO_INCREMENT COMMENT '主键' ,
`name`  varchar(255) NULL ,
`age`  int NULL ,
`from`  varchar(255) NULL ,
`createtime`  datetime NULL ,
`status`  integer(255) NULL DEFAULT 1 COMMENT '状态 1-正常 0-失效' ,
`delflag`  integer(255) NULL DEFAULT 1 COMMENT '删除标记 1-正常 0-删除' ,
PRIMARY KEY (`id`),
UNIQUE INDEX (`name`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='测试表'
;
INSERT INTO `test`.`test` (`id`, `name`, `age`, `fromto`, `createtime`, `status`, `delflag`) VALUES ('1', '张三', '22', '北京', '1970-01-01 08:00:00', '1', '1');
